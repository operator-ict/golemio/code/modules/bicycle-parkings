import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { BicycleParkings } from "#sch/index";
import { BicycleParkingsWorker } from "#ie/BicycleParkingsWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: BicycleParkings.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + BicycleParkings.name.toLowerCase(),
        queues: [
            {
                name: "refreshDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: BicycleParkingsWorker,
                workerMethod: "refreshDataInDB",
            },
            {
                name: "updateDistrict",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: BicycleParkingsWorker,
                workerMethod: "updateDistrict",
            },
        ],
    },
];

export { queueDefinitions };

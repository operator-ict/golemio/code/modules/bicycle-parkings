import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";

// MSO = Mongoose SchemaObject

const datasourceMSO: SchemaDefinition = {
    id: { type: Number, required: true },
    lat: { type: Number, required: true },
    lon: { type: Number, required: true },
    tags: { type: Object },
    type: { type: String },
};

const outputMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        district: { type: String },
        id: { type: Number, required: true },
        tags: [
            {
                _id: false,
                id: { type: String, required: true },
                value: { type: String },
            },
        ],
        updated_at: { type: Number, required: true },
    },
    type: { type: String, required: true },
};

const forExport = {
    datasourceMongooseSchemaObject: datasourceMSO,
    mongoCollectionName: "bicycleparkings",
    name: "BicycleParkings",
    outputMongooseSchemaObject: outputMSO,
};

export { forExport as BicycleParkings };

import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { BicycleParkings } from "#sch/index";
import { BicycleParkingsWorker } from "#ie/BicycleParkingsWorker";

describe("BicycleParkingsWorker", () => {
    let worker: BicycleParkingsWorker;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: number[];
    let data0: Record<string, any>;
    let data1: Record<string, any>;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        testData = [1, 2];
        testTransformedData = [1, 2];
        data0 = { properties: { id: 0 }, geometry: { coordinates: [0, 0] } };
        data1 = { properties: { id: 1 }, geometry: { coordinates: [1, 1] }, save: sandbox.stub().resolves(true) };

        worker = new BicycleParkingsWorker();

        sandbox.stub(worker["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(worker["model"], "save");
        sandbox.stub(worker, "sendMessageToExchange" as any);
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + BicycleParkings.name.toLowerCase();
        sandbox.stub(worker["model"], "findOneById").callsFake(() => Promise.resolve(data1));

        sandbox.stub(worker["cityDistrictsModel"], "findOne");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.refreshDataInDB({});
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["model"].save as SinonSpy);
        sandbox.assert.calledTwice(worker["sendMessageToExchange"] as SinonSpy);
        testTransformedData.map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateDistrict",
                JSON.stringify(f)
            );
        });
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["transformation"].transform as SinonSpy,
            worker["model"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by updateDistrict method (different geo)", async () => {
        await worker.updateDistrict({ content: Buffer.from(JSON.stringify(data0)) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, data0.properties.id);

        sandbox.assert.calledOnce(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.calledOnce(data1.save as SinonSpy);
    });

    it("should calls the correct methods by updateDistrict method (same geo)", async () => {
        data1 = {
            geometry: { coordinates: [0, 0] },
            properties: {
                address: { address_formatted: "a" },
                district: "praha-0",
                id: 1,
            },
            save: sandbox.stub().resolves(true),
        };
        await worker.updateDistrict({ content: Buffer.from(JSON.stringify(data0)) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, data0.properties.id);

        sandbox.assert.notCalled(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.notCalled(data1.save as SinonSpy);
    });
});
